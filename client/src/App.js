import axios from 'axios';
import React from "react";
import "./index.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import CreateAcc from "./pages/CreateAcc";
import DetailsPage from "./pages/DetailsPage";
import LandingPage from "./pages/LandingPage";
import SeasonsPage from "./pages/SeasonsPage";
import EpisodesPage from "./pages/EpisodesPage";
import EpisodeDetailsPage from "./pages/EpisodeDetailsPage";
import NavBar from "./components/NavBar";
import Nav from "./components/Nav";
import NoMatch from "./pages/NoMatch";
import UserPage from "./pages/UserPage";

const leftItems = [
  { as: "a", content: "FEAR NOT", key: "FEAR NOT" },
  { as: "a", content: "About", key: "About" },
  { as: "a", content: "Resources", key: "Resources" }
];
const rightItems = [
  { as: "a", content: "Login", key: "login" },
  { as: "a", content: "Register", key: "register" }
];

class App extends React.Component {
  state = {
    user: null
  }

  componentDidMount() {
    const token = localStorage.getItem('token');

    axios
        .get('/auth/verify', { headers: { Authorization: `bearer ${token}` } })
        .then((response) => {
            const data = response.data;
            this.setState({ user: data });
        })
        .catch((err) => {
            console.log(err.response.data);
            console.log('User not authenticated');
        });
}

CreateAcc = (props) => <CreateAcc user={this.state.user} {...props} />;
DetailsPage = (props) => <DetailsPage user={this.state.user} {...props} />;
LandingPage = (props) => <LandingPage user={this.state.user} {...props} />;
UserPage = (props) => <UserPage user={this.state.user} {...props} />;

  render () {
    return (
      <Router>
        <div>
        {/* <NavBar
         leftItems={leftItems} 
         rightItems={rightItems}
         user={this.state.user} /> */}
          <Nav user={this.state.user} />
          <Switch>
            <Route exact path="/" 
            component={this.LandingPage} 
            />
            <Route exact path="/details/:id" 
            component={this.DetailsPage} 
            />
            <Route exact path="/tv/:showID/seasons" 
            component={SeasonsPage} 
            />
            <Route exact path="/tv/:showID/season/:season_number/episodes" 
            component={EpisodesPage} 
            />
            <Route exact path="/tv/:showID/season/:season_number/episode/:episode_number" 
            component={EpisodeDetailsPage} 
            />
            <Route exact path="/create-account" 
             component={CreateAcc} 
            />
            <Route exact path="/my-account" 
             component={this.UserPage} 
            />
            <Route 
             component={NoMatch} 
            />
          </Switch>
        </div>
      </Router>
    );
  }
}



export default App;
