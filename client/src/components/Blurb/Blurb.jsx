import React from "react";
import { Header, Segment, List, Label, Grid } from 'semantic-ui-react';

class Blurb extends React.Component {
  state = {
    trigger: '',
    blurb: ''
  };


  render() {
    return (

      <Grid className='details-blurb-grid'>
        <Grid.Row>
          <Grid.Column>
            <Segment className='details-ui-segment1' attached='top' clearing secondary>
              <Header as='h2' floated='left' className='details-blurb-header'>
                {this.props.username}
              </Header>
              <Header as='h4' floated='right'>
                <List horizontal>
                  {this.props.data.Triggers.map(e =>
                    <List.Item
                      key={e.id}>
                      <Label size='large' color='yellow'>
                        {e.name}
                      </Label>
                    </List.Item>
                  )}
                </List>
              </Header>
            </Segment>
            <Segment className='details-ui-segment2' attached>
              {this.props.blurb}
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>

      // <div>
      //     <h4>Trigger<span>{this.props.username}</span></h4>
      //             <p>{this.props.blurb}</p>
      //     <ul>
      //       {this.props.data.Triggers.map(e => 
      //         <li key={e.id}>{e.name}</li>
      //       )}
      //     </ul>
      // </div>
    );
  }
}

export default Blurb;