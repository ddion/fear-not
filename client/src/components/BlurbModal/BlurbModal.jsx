import React from "react";
import Modal from "react-responsive-modal";
import InputBox from "../InputBox";
import API from "../../utils/API";
import Select from 'react-select';
import { withRouter } from 'react-router-dom';

class BlurbModal extends React.Component {
  state = {
    triggers: [],
    validationError: "",
    blurb: '',
    triggerOptions: [],
  };

  componentDidUpdate() {
    console.log(this.state);
  }

  handleSelectChange = (value) => this.setState({ triggers: value })

  componentDidMount() {
    API.findAllTriggers()
      .then(res => {
        // const value = res.value;
        for (let i = 0; i < res.data.length; i++) {
          const value = res.data[i].name;
          // This is an example of a spread operator 
          this.setState({ triggerOptions: [...this.state.triggerOptions, { value: res.data[i].id, label: value }] })
          console.log(this.state)
        }

        // How to take items out of an array in STATE
        // let newTriggerOptions = this.state.triggerOptions.filter(item => {
        //   if (item !== "hi") {
        //     return item
        //   }
        // });
        // this.setState({ triggerOptions: newTriggerOptions });
      });
  }

  handleChange = (e) => this.setState({ [e.target.name]: e.target.value });

  handleClick = (e) => {
    e.preventDefault()
    console.log(this.state.blurb)

    API.insertBlurb({
      blurb: this.state.blurb,
      UserUsername: this.props.username,
      MovieId: this.props.MovieId,
      triggers: this.state.triggers.map(e => e.value)
    })
      .then(res => {
        console.log(`I'm the response ${res}`);
        this.props.history.push('/details/' + this.props.MovieId);
        location.reload(); // eslint-disable-line
        // location.reload();
        // this.setState({
        //     blurb: '',
        //     open: false
        // }, () => console.log(this.state));
      });
  }

  render() {
    return (
      <Modal open={this.props.open} onClose={this.props.onCloseModal} center>
        <br />
        <h4 className="text-center">Add your feedback</h4>
        <form className="col-sm-12">
          <div className="">
            <label>
              Please select trigger(s):
          <Select
          // close = {false} to keep menu open when selecting triggers
                closeOnSelect={false}
                closeMenuOnSelect={false}
                options={this.state.triggerOptions}
                isMulti
                joinValues
                value={this.state.triggers}
                onChange={this.handleSelectChange}
                name="triggers"
                className="basic-multi-select"
                classNamePrefix="select" />
            </label>
            <InputBox type="text" name="blurb" label="Blurb" value={this.state.blurb} onChange={this.handleChange} />
          </div>
          <br />
          <button type='submit' className='col-sm-12 btn btn-primary' onClick={this.handleClick}>Submit</button>
        </form>
      </Modal>
    );
  }
}

export default withRouter(BlurbModal);