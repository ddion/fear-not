import React from "react";
import "./Episode.css";


const Episode = props => (
    <div className='row'>
        <div className="Episode-content">
            <div className="row">
                <h1 className="name">{props.name}<span id="air_date"><small>{props.air_date}</small></span></h1>
            </div>
            <br />
            <div className="row">
                <h4>Director(s): <span id="director">{props.directors}</span></h4>
            </div>
            <div className="row">
                <h4>Writer(s): <span id="writer">{props.writers}</span></h4>
            </div>
            <div className="row">
                <h4>Main Cast: <span id="actors">{props.cast}</span></h4>
            </div><div className="row">
                <h4>Guest Stars: <span id="actors">{props.guests}X</span></h4>
            </div>
            <br />
            <div className="row">
                <p id="plot">{props.overview}</p>
            </div>
        </div>
    </div>
);

export default Episode;