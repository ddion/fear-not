import React from "react";
import "./Episodes.css";
import { Link } from 'react-router-dom';
import { Segment, Grid, Divider } from 'semantic-ui-react';


const Episodes = props => (
    <Grid>
        <Grid.Row columns={2}>
            <Grid.Column mobile={16} tablet={6} computer={6}>
                    <div className="episode-still">
                    <Link to={"episode/" + props.episode_number}> 
                    <img src={`https://image.tmdb.org/t/p/w454_and_h254_bestv2${props.still_path}`}></img>
                    </Link>
                    </div>
            </Grid.Column>
            <Grid.Column verticalAlign='middle' mobile={16} tablet={10} computer={10}>
                <Grid.Row>
                <Link to={"episode/" + props.episode_number}>
                <h4 className='name'>
                {props.episode_number}.
                    {props.name}
                </h4>
                </Link>
                <span className='air_date'>
                    {props.air_date}
                </span>
                </Grid.Row>
                <Grid.Row>
                    <span className='overview'>{props.overview}</span>
                </Grid.Row>
            </Grid.Column>
        </Grid.Row>
        <Divider />
    </Grid>
    // <div className='row mt-5'>
    //     <div className='col-md-4'>
    //         <div className="row">
    //         {/* <Link to={"tv/" + props.tmdbShowID + "/season/" + props.id}></Link> */}
    //             <Link to={"episode/" + props.episode_number}> 
    //                 <div className="episode-still">
    //                     <img src={`https://image.tmdb.org/t/p/w454_and_h254_bestv2${props.still_path}`}></img>
    //                 </div>
    //             </Link>
    //         </div>
    //     </div>
    //     <div className='col-md-8'>
    //         <div className='row'>
    //         <Link to={"episode/" + props.episode_number}>
    //             <h4 className='name'>
    //             {props.episode_number}.
    //                 {props.name}
    //             </h4>
    //             </Link>
    //             <span className='air_date'>
    //                 {props.air_date}
    //             </span>
    //         </div>
    //         <div className='row'>
    //             <span className='overview'>{props.overview}</span>
    //         </div>

    //     </div>
    // </div>
);

export default Episodes;