import React from "react";
import "./Movie.css";
import { Grid } from 'semantic-ui-react';

const Movie = props => (
    // <Grid>
        <Grid.Column>
            <Grid.Row>
                <h1 className="title">{props.title}<span id="year"><small>{props.year}</small></span></h1>
            </Grid.Row>
            <Grid.Row>
                <span id="rated">{props.rated}</span> |
                <span id="runtime">{props.runtime}mins</span> |
                <span id="genres">{props.genres}</span>
            </Grid.Row>
            <br />
            <Grid.Row>
            <h4>Director(s): <span id="director">{props.directors}</span></h4>
            </Grid.Row>
            <Grid.Row>
            <h4>Writer(s): <span id="writer">{props.writers}</span></h4>
            </Grid.Row>
            <Grid.Row>
            <h4>Cast: <span id="actors">{props.cast}</span></h4> 
            </Grid.Row>
            <br />
            <Grid.Row>
            <p id="plot">{props.plot}</p>
            </Grid.Row>
        </Grid.Column>
    // </Grid>
    // <div className='row'>
    //     <div className="movie-content">
    //         <div className="row">
    //             <h1 className="title">{props.title}<span id="year"><small>{props.year}</small></span></h1>
    //         </div>
    //         <div className="row">
    //             <span id="rated">{props.rated}</span> |
    //     <span id="runtime">{props.runtime}mins</span> |
    //     <span id="genres">{props.genres}</span>
    //         </div>
    //         <br />
    //         <div className="row">
    //             <h4>Director(s): <span id="director">{props.directors}</span></h4>
    //         </div>
    //         <div className="row">
    //             <h4>Writer(s): <span id="writer">{props.writers}</span></h4>
    //         </div>
    //         <div className="row">
    //             <h4>Cast: <span id="actors">{props.cast}</span></h4>
    //         </div>
    //         <br />
    //         <div className="row">
    //             <p id="plot">{props.plot}</p>
    //         </div>
    //     </div>
    // </div>
);

export default Movie;