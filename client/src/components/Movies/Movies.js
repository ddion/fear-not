import React from "react";
import { Redirect, Link } from 'react-router-dom';
import "./Movies.css";
import { H4, H5 } from '../Headings'
import { Grid, Image } from 'semantic-ui-react'

// let year = props.year;
// var resYear = year.substr(0, 3);

const Movies = props => (
  
    <div className=""
    key={props.id}>
    
      <figure className="movie-figure">
      <Grid>
      <Grid.Column mobile={16} tablet={8} computer={4}>
        <Link to={"details/" + props.id}>
        <img src={`https://image.tmdb.org/t/p/w300${props.poster_path}`}
        className="movie-poster">
        </img>
        </Link>
        </Grid.Column>
      </Grid>
        <H4 className="movie-title">
        {props.title}
        </H4>
        <H5 className="movie-year">
        {props.release_date}
        </H5>
      </figure>
      
    </div>

  );

  export default Movies;