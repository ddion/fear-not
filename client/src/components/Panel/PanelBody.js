import React from 'react';

export const PanelBody = (props) => (
  <div className="ui grid centered" {...props}>
    <div className="row">
       {props.children}
		</div>
  </div>
)
