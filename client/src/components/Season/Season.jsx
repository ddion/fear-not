import React from "react";
import "./Season.css";
import { Link } from 'react-router-dom';
import { Segment, Grid, Divider } from 'semantic-ui-react'


const Season = props => (
    //    <Segment>
    <Grid>
        <Grid.Row columns={2}>
            <Grid.Column mobile={16} tablet={6} computer={6}>
                <div className="season-poster">
                    <Link to={"season/" + props.season_number + "/episodes"}>
                        <img src={`https://image.tmdb.org/t/p/w300${props.poster_path}`}></img>
                    </Link>
                </div>
            </Grid.Column>
            <Grid.Column verticalAlign='middle' mobile={16} tablet={10} computer={10}>
                <Grid.Row>
                    <Link to={"season/" + props.season_number + "/episodes"}>
                        <h4 className='name'>
                            {props.name}
                        </h4>
                    </Link>
                        <span className='air_date'>
                            {props.air_date}
                        </span>
                        |
                        <span className='episode_count'>
                            {props.episode_count}
                            episodes 
                        </span>
                </Grid.Row>
                <Grid.Row>
                    <span className='overview'>{props.overview}</span>
                </Grid.Row>
            </Grid.Column>
        </Grid.Row>
        <Divider />
    </Grid>

    // {/* </Segment> */}

    // <div className='row mt-5'>
    //     <div className='col-md-4'>
    //         <div className="row">
    //         {/* <Link to={"tv/" + props.tmdbShowID + "/season/" + props.id}></Link> */}
    //             <Link to={"season/" + props.season_number + "/episodes"}>
    //                 <div className="season-poster">
    //                     <img src={`https://image.tmdb.org/t/p/w200${props.poster_path}`}></img>
    //                 </div>
    //             </Link>
    //         </div>
    //     </div>
    //     <div className='col-md-8'>
    //         <div className='row'>
    //         <Link to={"season/" + props.season_number + "/episodes"}>
    //             <h4 className='name'>
    //                 {props.name}
    //             </h4>
    //             </Link>
    //             <span className='air_date'>
    //                 {props.air_date}
    //             </span>
    //             |
    //         <span className='episode_count'>
    //                 {props.episode_count}
    //                 episodes </span>
    //         </div>
    //         <div className='row'>
    //             <span className='overview'>{props.overview}</span>
    //         </div>

    //     </div>
    // </div>
);

export default Season;