import React from "react";
import { Redirect, Link } from 'react-router-dom';
import "./TvShows.css";
import { H4, H5 } from '../Headings';
import { Grid, Image } from 'semantic-ui-react'

// let year = props.year;
// var resYear = year.substr(0, 3);

const TvShows = props => (

  <div className=""
    key={props.id}>
    <figure className="tvshow-figure">
      <Grid>
        <Grid.Column mobile={16} tablet={8} computer={4}>
          <Link to={"tv/" + props.id + "/seasons"}>
            <img src={`https://image.tmdb.org/t/p/w300${props.poster_path}`}
              className="tvshow-poster">
            </img>
          </Link>
        </Grid.Column>
      </Grid>
      <H4 className="tvshow-title">
        {props.title}
      </H4>
      <H5 className="tvshow-year">
        {props.release_date}
      </H5>
    </figure>
  </div>

);

export default TvShows;