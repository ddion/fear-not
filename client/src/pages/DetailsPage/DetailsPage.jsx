import React from "react";
import BlurbModal from "../../components/BlurbModal";
import Movie from "../../components/Movie";
import imdbAPI from "../../utils/imdbAPI";
import API from "../../utils/API";
import './DetailsPage.css';
import Blurb from "../../components/Blurb";
import { Panel, PanelBody, PanelHeading } from '../../components/Panel';
import TMDB from "../../utils/TmdbAPI";
import { 
    Grid, 
    Image,
    Container } from 'semantic-ui-react'

class DetailsPage extends React.Component {
    state = {
        blurbs: [],
        cast: '',
        directors: '',
        genre: '',
        open: false,
        rating: '',
        results: {},
        writers: '',
    };

    componentDidMount() {
        console.log("Component did mount")
        // console.log(this.props)
        console.log(this.props.match.params.id)
        var tmdbID = this.props.match.params.id
        TMDB.getMoviebyID(tmdbID).then(res => {
            console.log("Detail Res", res);
            this.setState({
                directors: res.data.credits.crew.filter(e => e.department === 'Directing').slice(0, 3).map(e => e.name).join(', '),
                cast: res.data.credits.cast.map(e => e.name).slice(0, 6).join(', '),
                genre: res.data.genres.map(e => e.name).join(', '),
                results: res.data,
                rating: res.data.release_dates.results.filter(e => e.iso_3166_1 === 'US')[0].release_dates[0].certification || 'Not Rated',
                writers: res.data.credits.crew.filter(e => ['Screenplay', 'Writing'].includes(e.department)).slice(0, 3).map(e => e.name).join(', '),
            }, () => console.log(this.state.results));
        })
        API.findBlurb(tmdbID).then(res => {
            // console.log(tmdbID);
            // console.log(`this is the result.id ${this.props.match.params.id}`)
            console.log("DB res", res);
            this.setState({
                blurbs: res.data
            }, () => console.log(this.state.blurbs));
        })

    };

    handleClick = event => {
        console.log("HandleClickEvent")
        if (!this.props.user) {
            alert("You need to login to access this feature");
            console.log("You need to login to access this feature");
        }
        else {
            console.log("You feature");
            this.onOpenModal()

        }
    };

    onOpenModal = () => {
        this.setState({ open: true });
        API.insertMovie(this.state.results.title, this.state.results.id).then(res => {
            console.log(res);
        })

    };

    onCloseModal = () => {
        this.setState({ open: false });
    };


    render() {
        return (
            <Container>
                <Grid className='details-grid' divided='vertically'>

                    <Grid.Row columns={2}>

                        <Grid.Column 
                        // className="ui grid centered" 
                        mobile={16} tablet={16} computer={8}>
                            <Grid.Row className='details-poster-row'>
                                <div className="details-poster">
                                    <img src={`https://image.tmdb.org/t/p/w300${this.state.results.poster_path}`}></img>
                                </div>
                            </Grid.Row>
                            <Grid.Row className='details-blurb-modal-row'>
                                <div className="blurb-modal">
                                    <button onClick={this.handleClick} className="btn btn-primary">Add Feed Back</button>
                                    <BlurbModal
                                        username={this.props.user && this.props.user.username}
                                        MovieId={this.state.results.id}
                                        open={this.state.open} onCloseModal={this.onCloseModal} />
                                </div>
                            </Grid.Row>
                        </Grid.Column>

                        <Grid.Column verticalAlign='middle' mobile={16} tablet={16} computer={8}>
                            <div className="movie-info">
                                <Movie
                                    title={this.state.results.title}
                                    year={this.state.results.release_date}
                                    rated={this.state.rating}
                                    runtime={this.state.results.runtime}
                                    genres={this.state.genre}
                                    directors={this.state.directors}
                                    writers={this.state.writers}
                                    cast={this.state.cast}
                                    plot={this.state.results.overview}
                                />
                            </div>
                        </Grid.Column>

                    </Grid.Row>







                    <Grid.Row columns={1}>

                        <Grid.Column width={16}>
                        <h4>Known Blurbs</h4>
                        {/* <Panel>
                            <PanelBody> */}
                                {
                                    this.state.blurbs.map((blurb, i) => (
                                        <Blurb
                                            key={i}
                                            blurb={blurb.blurb}
                                            username={blurb.UserUsername}
                                            data={blurb}
                                        />
                                    )
                                    )
                                }
                            {/* </PanelBody>
                        </Panel>   */}
                        </Grid.Column>

                    </Grid.Row>

                </Grid>
            </Container>
            // <div className="container">
            //     <div className="row">
            //         <div className="col-md-6">
            //             <div className="row">
            //                 <div className="movie-poster">
            //                     <img src={`https://image.tmdb.org/t/p/w300${this.state.results.poster_path}`}></img>
            //                 </div>
            //             </div>
            //             <div className="row">
            //                 <div className="blurb-modal">
            //                     <button onClick={this.handleClick} className="btn btn-primary">Add Feed Back</button>
            //                     <BlurbModal
            //                         username={this.props.user && this.props.user.username}
            //                         MovieId={this.state.results.id}
            //                         open={this.state.open} onCloseModal={this.onCloseModal} />
            //                 </div>
            //             </div>
            //         </div>
            //         <div className="col-md-6">
            //             <div className="movie-info">
            //                 <Movie
            //                     title={this.state.results.title}
            //                     year={this.state.results.release_date}
            //                     rated={this.state.rating}
            //                     runtime={this.state.results.runtime}
            //                     genres={this.state.genre}
            //                     directors={this.state.directors}
            //                     writers={this.state.writers}
            //                     cast={this.state.cast}
            //                     plot={this.state.results.overview}
            //                 />
            //             </div>
            //         </div>

            //         {/* <div className="col-md-2">
                       
            //         </div> */}
            //     </div>
            //     {/* <div className="row">
                   
            //     </div> */}
            //     <div className="row">
            //         <div className="col-md-12">
            //             <h4>Known Blurbs</h4>
            //             <Panel>
            //                 <PanelBody>
            //                     {
            //                         this.state.blurbs.map((blurb, i) => (
            //                             <Blurb
            //                                 key={i}
            //                                 blurb={blurb.blurb}
            //                                 username={blurb.UserUsername}
            //                                 data={blurb}
            //                             />
            //                         )
            //                         )
            //                     }
            //                 </PanelBody>
            //             </Panel>
            //             {/* blurb component */}
            //             {/* <Blurb /> */}
            //         </div>
            //     </div>
            // </div>
        )
    }
}

export default DetailsPage;