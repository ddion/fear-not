import React from "react";
import "./Season.css";
import { Link } from 'react-router-dom';
import { Segment, Grid, Divider } from 'semantic-ui-react';


const Season = props => (
    <Container>
    <Grid className='details-grid' divided='vertically'>

        <Grid.Row columns={2}>

            <Grid.Column 
            // className="ui grid centered" 
            mobile={16} tablet={16} computer={8}>
                <Grid.Row className='details-still-row'>
                <div className="tvshow-still">
                                <img src={`https://image.tmdb.org/t/p/w454_and_h254_bestv2${this.state.results.still_path}`}></img>
                            </div>
                </Grid.Row>
                <Grid.Row className='details-blurb-modal-row'>
                     {/* <div className="blurb-modal">
                                <button onClick={this.handleClick} className="btn btn-primary">Add Feed Back</button>
                                <BlurbModal
                                    username={this.props.user && this.props.user.username}
                                    MovieId={this.state.results.id}
                                    open={this.state.open} onCloseModal={this.onCloseModal} />
                            </div> */}
                </Grid.Row>
            </Grid.Column>

            <Grid.Column verticalAlign='middle' mobile={16} tablet={16} computer={8}>
            <div className="episode-info">
                            <Episode
                                name={this.state.results.name}
                                air_date={this.state.results.air_date}
                                directors={this.state.directors}
                                writers={this.state.writers}
                                cast={this.state.cast}
                                guests={this.state.guests}
                                overview={this.state.results.overview}
                            />
                        </div>
            </Grid.Column>

        </Grid.Row>







        <Grid.Row columns={1}>

            <Grid.Column width={16}>
            <h4>Known Blurbs</h4>
            {/* <Panel>
                <PanelBody> */}
                    {/* {
                                    this.state.blurbs.map((blurb, i) => (
                                        <Blurb
                                            key={i}
                                            blurb={blurb.blurb}
                                            username={blurb.UserUsername}
                                            data={blurb}
                                        />
                                    )
                                    )
                                } */}
                {/* </PanelBody>
            </Panel>   */}
            </Grid.Column>

        </Grid.Row>

    </Grid>
</Container>
);

export default Season;