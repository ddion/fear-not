import React from "react";
import BlurbModal from "../../components/BlurbModal";
import Episode from "../../components/Episode";
import API from "../../utils/API";
import './EpisodeDetailsPage.css';
import Blurb from "../../components/Blurb";
import { Panel, PanelBody, PanelHeading } from '../../components/Panel';
import TMDB from "../../utils/TmdbAPI";
import {
    Grid,
    Image,
    Container
} from 'semantic-ui-react'

class EpisodeDetailsPage extends React.Component {
    state = {
        blurbs: [],
        cast: '',
        directors: '',
        guests: '',
        open: false,
        results: {},
        writers: '',
    };

    componentDidMount() {
        console.log("Component did mount")
        var showID = this.props.match.params.showID
        var seasonNumber = this.props.match.params.season_number
        var episodeNumber = this.props.match.params.episode_number
        TMDB.getEpisodeDetails(showID, seasonNumber, episodeNumber).then(res => {
            console.log("Detail Res", res);
            this.setState({
                directors: res.data.credits.crew.filter(e => e.department === 'Directing').slice(0, 3).map(e => e.name).join(', ') || 'No Director added yet',
                cast: res.data.credits.cast.map(e => e.name).slice(0, 6).join(', '),
                guests: res.data.guest_stars.map(e => e.name).slice(0, 6).join(', '),
                results: res.data,
                writers: res.data.credits.crew.filter(e => ['Screenplay', 'Writing'].includes(e.department)).slice(0, 3).map(e => e.name).join(', ') || 'No Writer added yet',
            }, () => console.log(this.state.results));
        })
        // API.findBlurb(tmdbID).then(res => {
        //     // console.log(tmdbID);
        //     // console.log(`this is the result.id ${this.props.match.params.id}`)
        //     console.log("DB res", res);
        //     this.setState({
        //         blurbs: res.data
        //     }, () => console.log(this.state.blurbs));
        // })

    };

    handleClick = event => {
        console.log("HandleClickEvent")
        if (!this.props.user) {
            alert("You need to login to access this feature");
            console.log("You need to login to access this feature");
        }
        else {
            console.log("You feature");
            this.onOpenModal()

        }
    };

    onOpenModal = () => {
        this.setState({ open: true });
        // API.insertMovie(this.state.results.title, this.state.results.id).then(res => {
        //     console.log(res);
        // })

    };

    onCloseModal = () => {
        this.setState({ open: false });
    };


    render() {
        return (
            <Container>
                <Grid className='details-grid' divided='vertically'>

                    <Grid.Row columns={2}>

                        <Grid.Column
                            // className="ui grid centered" 
                            mobile={16} tablet={16} computer={8}>
                            <Grid.Row className='details-still-row'>
                                <div className="tvshow-still">
                                    <img src={`https://image.tmdb.org/t/p/w454_and_h254_bestv2${this.state.results.still_path}`}></img>
                                </div>
                            </Grid.Row>
                            <Grid.Row className='details-blurb-modal-row'>
                                {/* <div className="blurb-modal">
                                <button onClick={this.handleClick} className="btn btn-primary">Add Feed Back</button>
                                <BlurbModal
                                    username={this.props.user && this.props.user.username}
                                    MovieId={this.state.results.id}
                                    open={this.state.open} onCloseModal={this.onCloseModal} />
                            </div> */}
                            </Grid.Row>
                        </Grid.Column>

                        <Grid.Column verticalAlign='middle' mobile={16} tablet={16} computer={8}>
                            <div className="episode-info">
                                <Episode
                                    name={this.state.results.name}
                                    air_date={this.state.results.air_date}
                                    directors={this.state.directors}
                                    writers={this.state.writers}
                                    cast={this.state.cast}
                                    guests={this.state.guests}
                                    overview={this.state.results.overview}
                                />
                            </div>
                        </Grid.Column>

                    </Grid.Row>







                    <Grid.Row columns={1}>

                        <Grid.Column width={16}>
                            <h4>Known Blurbs</h4>
                            {/* <Panel>
                <PanelBody> */}
                            {/* {
                                    this.state.blurbs.map((blurb, i) => (
                                        <Blurb
                                            key={i}
                                            blurb={blurb.blurb}
                                            username={blurb.UserUsername}
                                            data={blurb}
                                        />
                                    )
                                    )
                                } */}
                            {/* </PanelBody>
            </Panel>   */}
                        </Grid.Column>

                    </Grid.Row>

                </Grid>
            </Container>




            // <div className="container">
            //     <div className="row">
            //         <div className="col-md-6">
            //             <div className="row">
            //                 <div className="tvshow-still">
            //                     <img src={`https://image.tmdb.org/t/p/w454_and_h254_bestv2${this.state.results.still_path}`}></img>
            //                 </div>
            //             </div>
            //             <div className="row">
            //                 {/* <div className="blurb-modal">
            //                     <button onClick={this.handleClick} className="btn btn-primary">Add Feed Back</button>
            //                     <BlurbModal
            //                         username={this.props.user && this.props.user.username}
            //                         MovieId={this.state.results.id}
            //                         open={this.state.open} onCloseModal={this.onCloseModal} />
            //                 </div> */}
            //             </div>
            //         </div>
            //         <div className="col-md-6">
            //             <div className="movie-info">
            //                 <Episode
            //                     name={this.state.results.name}
            //                     air_date={this.state.results.air_date}
            //                     directors={this.state.directors}
            //                     writers={this.state.writers}
            //                     cast={this.state.cast}
            //                     guests={this.state.guests}
            //                     overview={this.state.results.overview}
            //                 />
            //             </div>
            //         </div>


            //     </div>

            //     <div className="row">
            //         <div className="col-md-12">
            //             <h4>Known Blurbs</h4>
            //             <Panel>
            //                 <PanelBody>
            //                     {/* {
            //                         this.state.blurbs.map((blurb, i) => (
            //                             <Blurb
            //                                 key={i}
            //                                 blurb={blurb.blurb}
            //                                 username={blurb.UserUsername}
            //                                 data={blurb}
            //                             />
            //                         )
            //                         )
            //                     } */}
            //                 </PanelBody>
            //             </Panel>
            //         </div>
            //     </div>
            // </div>
        )
    }
}

export default EpisodeDetailsPage;