import React from "react";
import './EpisodesPage.css';
import Episodes from "../../components/Episodes";
import TMDB from "../../utils/TmdbAPI";
import { Container } from 'semantic-ui-react'

class EpisodesPage extends React.Component {
    state = {
        id: '',
        results: [],
        // tmdbShowID: this.props.match.params.id
    }

    componentDidMount() {
        console.log("Component Did Mount");

        var showID = this.props.match.params.showID
        var seasonNumber = this.props.match.params.season_number
        TMDB.getSeasonEpisodes(showID, seasonNumber).then(res => {
            console.log("Episodes Res", res);
            this.setState({
                results : res.data.episodes
            }, () => console.log(this.state))
        })
    }

    render() {
        return (
            <Container className='episodes-page-container'>
                {
                    this.state.results.map((episodes, i) => (
                        <Episodes
                        key={i}
                        id = {episodes.id}
                        episode_number = {episodes.episode_number}
                        name = {episodes.name}
                        still_path = {episodes.still_path}
                        air_date = {episodes.air_date}
                        // episode_count = {episodes.episode_count}
                        overview = {episodes.overview}
                        tmdbShowID = {this.state.tmdbShowID}
                        />
                    ))
                }
            </Container>
        )
    }

}

export default EpisodesPage;