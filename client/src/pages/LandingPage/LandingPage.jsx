import imdbAPI from "../../utils/imdbAPI";
import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from 'react-router-dom';
import InputBox from "../../components/InputBox";
// import Movie from "../../components/Movie";
import Movies from "../../components/Movies";
import { Panel, PanelBody, PanelHeading } from '../../components/Panel';
import TMDB from "../../utils/TmdbAPI";
// import Select from "react-select";
import { Button, Select, Input } from 'semantic-ui-react'
import TvShows from "../../components/TvShows/TvShows";

const searchOptions = [
    { value: 'Movie', label: 'Movie' },
    { value: 'Tv Show', label: 'Tv Show' }
]

const options = [
    { key: 'Movie', text: 'Movie', value: 'Movie' },
    { key: 'Tv Show', text: 'Tv Show', value: 'Tv Show' },
]

class LandingPage extends React.Component {
    state = {
        catagory: options[0].value,
        // catagory: searchOptions[0],
        sq: "",//search query entered by user
        id: "",
        submitted: false,
        results: [],//array of results returned from api
        // previousSearch: {},//previous search term saved after search completed
        // noResults: false,//boolean used as flag for conditional rendering
    };

    onCatagoryChange = (e, data) => {
        console.log(data.value);
        this.setState({ catagory: data.value });
      }
    componentDidMount() {
        TMDB.getPopularMovie()
            .then(res => {
                console.log("RES", res);
                this.setState({
                    results: res.data.results
                }, () => console.log(this.state))
            });
    }

    handleSelectChange = (value) => this.setState({ catagory: value }, () => console.log(this.state))

    handleChange = (e) => this.setState({ [e.target.name]: e.target.value });

    handleSubmit = async (e) => {
        e.preventDefault()
        this.setState({
            // submitted: true
        })

        console.log(this.state);
        if (this.state.catagory === "Movie") {
            TMDB.searchMovie(this.state.sq).then(res => {
                // console.log("RES", res);

                this.setState({
                    results: res.data.results,
                    // submitted: false
                }, () => console.log(this.state));
            });
        } else
            if (this.state.catagory === "Tv Show") {
                TMDB.searchTvShow(this.state.sq).then(res => {
                    // console.log("RES", res);

                    this.setState({
                        results: res.data.results,
                        // submitted: false
                    }, () => console.log(`THESE ARE TV RESULTS ${this.state}`));
                });

            }

    }


    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">

                        <div className="card">
                            <div className="card-body">
                                <span className="card-title"><h3>Search Movie</h3></span>

                                <form className="container">
                                    <Input fluid type='text' name="sq" value={this.state.sq} onChange={this.handleChange} placeholder='Search...' action>
                                        <input />
                                        <Select compact options={options} 
                                        // defaultValue='Movie' 
                                        onChange={this.onCatagoryChange}
                                        value={this.state.catagory}
                                        />
                                        <Button onClick={this.handleSubmit} type='submit'>Search</Button>
                                    </Input>
                                    {/* <InputBox type="text" name="sq" label="Search" value={this.state.sq} onChange={this.handleChange} />
                                    <Select
                                        // defaultValue={searchOptions[0]}
                                        options={searchOptions}
                                        onChange={this.handleSelectChange}
                                        value={this.state.catagory}
                                    />
                                    <button type="submit" className="btn btn-primary" onClick={this.handleSubmit}>Submit</button> */}
                                </form>

                            </div>
                        </div>

                    </div>
                </div>
                <Panel>
                    <PanelBody>
                        {/* {
                            this.state.results.map((movie, i) => (
                                <Movies
                                    key={i}
                                    id={movie.id}
                                    title={movie.title}
                                    poster_path={movie.poster_path}
                                    release_date={movie.release_date}
                                />
                            )
                            )
                        } */}
                        {this.state.catagory === "Movie" ? (

                            this.state.results.map((movie, i) => (
                                <Movies
                                    key={i}
                                    id={movie.id}
                                    title={movie.title}
                                    poster_path={movie.poster_path}
                                    release_date={movie.release_date}
                                />
                            )
                            )

                        ) : (
                                this.state.results.map((tvshow, i) => (
                                    <TvShows
                                        key={i}
                                        id={tvshow.id}
                                        title={tvshow.name}
                                        poster_path={tvshow.poster_path}
                                        release_date={tvshow.first_air_date}
                                    />
                                )
                                )
                            )}
                    </PanelBody>
                </Panel>
            </div>
        );
    }
}

export default LandingPage;