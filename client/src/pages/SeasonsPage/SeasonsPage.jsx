import React from "react";
import './SeasonsPage.css';
import Season from "../../components/Season";
import TMDB from "../../utils/TmdbAPI";
import { Container } from 'semantic-ui-react'

class SeasonsPage extends React.Component {
    state = {
        id: '',
        results: [],
        tmdbShowID: this.props.match.params.showID
    }

    componentDidMount() {
        console.log("Component Did Mount");
        // console.log(this.props.title);

        var tmdbShowID = this.props.match.params.showID
        TMDB.getSeasonsbyID(tmdbShowID).then(res => {
            console.log("Season Res", res);
            this.setState({
                results : res.data.seasons
            }, () => console.log(this.state))
        })
    }

    render() {
        return (
            <Container className='season-page-container'>
                {
                    this.state.results.map((season, i) => (
                        <Season
                        key={i}
                        id = {season.id}
                        season_number = {season.season_number}
                        name = {season.name}
                        poster_path = {season.poster_path}
                        air_date = {season.air_date}
                        episode_count = {season.episode_count}
                        overview = {season.overview}
                        tmdbShowID = {this.state.tmdbShowID}
                        />
                    ))
                }
            </Container>
        )
    }

}

export default SeasonsPage;