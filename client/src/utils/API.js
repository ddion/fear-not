import axios from "axios";

export default {
  // insert movie with id and BlurbID
  insertMovie: function(title, id) {
    return axios.post("/api/route/movie/" + title + "/" + id);
  },
  // find movies with title in db
  findMovie: function(title) {
    return axios.get("/api/route/movie/" + title);
  },
  // post anew blurb
  insertBlurb: function(blurb) {
    console.log(`api post blurb ${blurb}`)
    return axios.post("/api/route/blurb/", blurb);
  },
  // update a blurb in the db
  updateBlurb: function(blurb) {
    return axios.put("/api/route/movies", blurb);
  },
  // find blurbs attached to movies
  findBlurb: function(tmdbID) {
    console.log(`Here we are in API.js Line 23 ${tmdbID}`)
    return axios.get("/api/route/blurb/" + tmdbID)
    ;
  },
  // find all triggers in db
  findAllTriggers: function() {
    return axios.get("/api/route/triggers/");
  },
};
