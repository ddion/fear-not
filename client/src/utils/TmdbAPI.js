import axios from "axios";

export default {
    // Finds the most popular movies
    getPopularMovie: function() {
        return axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=ffbaf07888bd399edac14591a6cfe46d`)

        // const url = `https://api.themoviedb.org/3/movie/popular?api_key=ffbaf07888bd399edac14591a6cfe46d`;
        // fetch (url)
        //   .then(response => response.json())
        //   .then(data => {
        //     this.setState({
        //       movies: data.results
        //     })
        //   });
    },
    searchMovie: function(query) {
        return axios.get(`https://api.themoviedb.org/3/search/movie?api_key=ffbaf07888bd399edac14591a6cfe46d&query=${query}`)
    },
    getMoviebyID: function(id) {
        return axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=ffbaf07888bd399edac14591a6cfe46d&append_to_response=credits,external_ids,release_dates`)
    },
    searchTvShow: function(query) {
        return axios.get(`https://api.themoviedb.org/3/search/tv?api_key=ffbaf07888bd399edac14591a6cfe46d&query=${query}`)
    },
    getSeasonsbyID: function(id) {
        return axios.get(`https://api.themoviedb.org/3/tv/${id}?api_key=ffbaf07888bd399edac14591a6cfe46d`)
    },
    getSeasonEpisodes: function(showID, seasonNumber) {
        return axios.get(`https://api.themoviedb.org/3/tv/${showID}/season/${seasonNumber}?api_key=ffbaf07888bd399edac14591a6cfe46d`)
    },
    getEpisodeDetails: function(showID, seasonNumber, episodeNumber) {
        return axios.get(`https://api.themoviedb.org/3/tv/${showID}/season/${seasonNumber}/episode/${episodeNumber}?api_key=ffbaf07888bd399edac14591a6cfe46d&append_to_response=images,credits,external_ids,release_dates`)
    }
  };