var db = require("../models");


module.exports = {
    //movie methods, insert, update Blurb, select
    //route works
    insertMovie: function(req,res) {
        console.log(req.params);
        var title = req.params.title;
        var id = req.params.id;
        var BlurbID = req.params.BlurbID;

        db.sequelize
            .query(
                "insert into movies (title, id) values (?, ?)",
                {replacements: [title, id]}
            ).then(dbModel => res.json(dbModel))
        .catch(err => {
            console.log(err);
            res.status(500).json({ err: err.message })
        });
    },
    // updateMovieBlurb: function(req, res) {
    //     db.Movie.update(req.body, {
    //         where: {
    //             id: req.params.id
    //         }
    //     }).then(dbModel => res.json(dbModel))
    //     .catch(err => res.status(500).json(err));
    // },
    //route works
    findMovie: function(req,res) {
        db.Movie.findOne({
            where: {title: req.params.title},
            attributes: ['title', 'id']
        }).then(dbModel => res.json(dbModel))
        .catch(err => {
            console.log(err);
            res.status(500).json({ err: err.message })
        });
    },
    // blurb methods, create blurb, update blurb
    //route works in postman
    insertBlurb: function(req,res) {
        db.Blurb.create({
            blurb: req.body.blurb,
            UserUsername: req.body.UserUsername,
            MovieId: req.body.MovieId
        })
        .then(dbModel => dbModel.addTriggers(req.body.triggers))
        .then(() => res.json({ status: 'ok' }))
        .catch(err => {
            console.log(err);
            res.status(500).json({ err: err.message })
        });
    },
    //works in postman
    updateBlurb: function(req,res) {
        db.Blurb.update(req.body, {
            where: {
                id: req.body.id
            }
        }).then(dbModel => res.json(dbModel))
        .catch(err => res.status(500).json(err => {
            console.log(err);
            res.status(500).json({ err: err.message })
        }));
    },

    //route works
    findBlurb: function(req, res) {
        console.log(`Hello from controller.js line 59`)
        var MovieId = req.params.MovieId
        db.Blurb.findAll({
            where: {MovieId: MovieId},
            include: [{
                model: db.Trigger,
                through: {
                  attributes: ['blurb_triggers']
                }
              }]
        }).then(dbModel => res.json(dbModel))
        .catch(err => {
            console.log(err);
            res.status(500).json({ err: err.message })
        });
    },

    findAllTriggers: function(req,res) {
        db.Trigger.findAll({})
        .then(dbModel => res.json(dbModel))
        .catch(err => {
            console.log(err);
            res.status(500).json({ err: err.message })
        });
    }
}