module.exports = function(sequelize, DataTypes) {
    var Trigger = sequelize.define("Trigger", {
      name: DataTypes.STRING
    });

    Trigger.associate = function(models) {
      Trigger.belongsToMany(models.Blurb, { through: 'blurb_triggers' });
    }
  
    return Trigger;
  };